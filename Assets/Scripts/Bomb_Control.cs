﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb_Control : MonoBehaviour
{
    [Tooltip("velocidad de caida")]
    public float speed = 0.5f;

    [Tooltip("Maxima y permitida")]
    //public float yLimit = -1.5f;

    public ParticleSystem Explosion;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 currentPos = rb.position;
        currentPos.y -= speed * Time.deltaTime;
        rb.MovePosition(currentPos);
        /*
        Transform t = GetComponent<Transform>();
        Vector3 pos = t.position;
        //pos.y = pos.y - speed * Time.deltaTime;
        t.position = pos;

       /* GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            Collider collider = player.GetComponent<Collider>();
            if (collider != null)
            {
                if (collider.bounds.Contains(transform.position))
                {
                    Catched();
                    return;
                }
            }
        }
       */
       /* if (Grounded())
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Bomb"))
            {
                Bomb_Control bc;
                bc = go.GetComponent<Bomb_Control>();
                if (bc !=null)
                {
                    bc.Explode();
                }
            }
        }*/
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Catched();
            return;
        }
        else
        {
            GameManager.Instance.BombGrounded();
        }
    }


    /*void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Catched();
            return;
        }
    }

   /* bool Grounded()
    {
        return transform.position.y <= yLimit;
    }
   */
   public void Explode()
    {
        Destroy(this.gameObject);
        //GameManager.Instance.menos();   esta resta puntos, no usar de momento
        Explot();
    }

    void Catched()
    {
        GameManager.Instance.BombCatched();
        Destroy(gameObject);

    }

    void Explot()
    {
    ParticleSystem explosionEffect = Instantiate(Explosion) as ParticleSystem;

        explosionEffect.transform.position = transform.position;
        //play it
        explosionEffect.loop = false;
        explosionEffect.Play();

        //destroy the particle system when its duration is up, right
        //it would play a second time.
        Destroy(explosionEffect.gameObject, explosionEffect.duration-1.0f);
        //destroy our game object
        //Destroy(gameObject);
    }


}
