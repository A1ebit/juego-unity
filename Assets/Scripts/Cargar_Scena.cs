﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Cargar_Scena : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Juego()
    {
        SceneManager.LoadScene("Main");
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void Puntos()
    {
        SceneManager.LoadScene("HighscoreBoard - Copy");
    }
}
