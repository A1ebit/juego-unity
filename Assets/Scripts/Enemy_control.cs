﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy_control : MonoBehaviour
{
    public Animator anim;

    [Tooltip("bomb que se lanza OPCION1")]
    public GameObject bomb1;

    [Tooltip("bomb que se lanza OPCION2")]
    public GameObject bomb2;

    [HideInInspector]
    public Transform spawnPosition;

    [HideInInspector]
    public float bombRate = 0.5f;

    [HideInInspector]
    public float bombspeed = 3.0f;

    [HideInInspector]
    public float speed = 3.0f;

    [Tooltip("Min X desplazamiento")]
    public float xMin = -10;

    [Tooltip("Max X desplazamiento")]
    public float xMax = 10;

    [HideInInspector]
    public int bombCount = 5;

    private float currentDir; // 1 / -1 -> izquierda o derecha
    private float nextBombTime;
    private int remainingBombs;

    // Start is called before the first frame update
    void Start()
    {
        //StartBombing();
    }

    public void StartBombing()
    {
        if (remainingBombs!=0)
        {
            Debug.LogError("StartBombing()... " + remainingBombs);

        }
        else
        {
            if (bombRate <= 0.0)
            {
                Debug.LogWarning("Bombrate debe ser positivo");
                bombRate = 1.0f;
            }
            if (bombCount == 0)
            {
                Debug.LogWarning("Bomb count  es " + bombCount);
                enabled = false;
                return;
            }
            remainingBombs = bombCount;
            currentDir = 1.0f;
            // randomlyChangeDirection();
            nextBombTime = Time.time;
            enabled = true;
        }
    }
    
    
    
    
    // Update is called once per frame
    void Update()
    {

        Vector3 p = transform.position;
        p.x = p.x + (speed * currentDir * Time.deltaTime);
        if (p.x < xMin)
        {
            p.x = xMin;
            currentDir = currentDir * -1;
        }
        else if (p.x > xMax)
        {
            p.x = xMax;
            currentDir = currentDir * -1;
        }

        transform.position = p;
        if (Time.time >= nextBombTime)
        {
            if (remainingBombs==0)
            {
                enabled = false;
                return;
            }
            else
            {
                remainingBombs--;
                Bombard();
                randomlyChangeDirection();
                nextBombTime = nextBombTime + (1.0f / bombRate);  // nextbombtime += 1.0f / bombrate
            }

        }
    }

    void Bombard() 
    {
        int i = Random.Range(1, 6);
        GameObject bomb;
        if (i > 3)
        {
            bomb = bomb1;
            Instantiate(bomb, spawnPosition.position, spawnPosition.rotation);

        }
        else
        {
            bomb = bomb2;
            Instantiate(bomb, spawnPosition.position, Quaternion.Euler(-60, 180, -180));

        }
          Bomb_Control bc;
          bc = bomb.GetComponent<Bomb_Control>();
          bc.speed = bombspeed;
    }

    void randomlyChangeDirection()
    {
        if (Random.value <0.5)
        {
            currentDir *= -1;
        }
    }

    public void StopBombing()
    {
        remainingBombs = 0;
        enabled = false;
    }

}
