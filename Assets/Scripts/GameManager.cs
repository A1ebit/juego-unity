﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager : Error");
            }
            return _instance;
        }
    }
    //[HideInInspector]
    public AudioSource inicio;

    //[HideInInspector]
    public AudioSource Chao;

    public Animator anim;

    public void death()
    {
        anim.SetBool("life", false);
    }

    public void revive()
    {
        anim.SetBool("life", true);
    }

    public void PlayInicio()
    {
        inicio.Play();
    }

    public void PlayFinal()
    {
        Chao.Play();
    }
    void Awake()
    {
        Cursor.visible = false;

        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }


    [System.Serializable]
    public class BurstConfig
    {
        public float bombRate = 1f;

        public float bombspeed = 3.0f;

        public float speed = 3.0f;

        public int bombCount = 5;

        public int pointsPerBomb = 1;
    }
    public BurstConfig[] bursts;
    [HideInInspector]
    public int currentBurt = 0;
    [HideInInspector]
    public int lives = 3;
    [HideInInspector]
    public int currentPoints = 0;
    public int points
    {
    get
        {
            return currentPoints;
        }
    }
   

    void OnDestroy()
    {
        if (this == _instance)
        {
            _instance = null;
        }
    }

    void Update()
    {
        Papascha();
    }

    public void NextBurst()
    {
        if (GameObject.FindGameObjectWithTag("Bomb"))
        {
            Debug.LogWarning("hay bombas en juego");
            return;
        }

        Enemy_control ec = GetEnemy();

        if (ec == null)
        {
            Debug.LogWarning("No enemy");

            return;
        }
        Debug.LogWarning("nivel " + currentBurt);

        if (currentBurt == bursts.Length )
        {
            --currentBurt;
        }
        ec.bombRate = bursts[currentBurt].bombRate;
        ec.bombspeed = bursts[currentBurt].bombspeed;
        ec.speed = bursts[currentBurt].speed;
        ec.bombCount = bursts[currentBurt].bombCount;

        ++currentBurt;
        PlayInicio();
        revive();
        Invoke("Empezar", 1.5f);
    }

    public void Restart()
    {
        lives = 3;
        GetPlayer().setNumBars(lives);
        currentBurt = 0;
        currentPoints = 0;
    }

    void Empezar()
    {
        Enemy_control ec = GetEnemy();
        ec.StartBombing();
    }

    static Enemy_control GetEnemy()
    {
       GameObject enemy;
       enemy = GameObject.FindGameObjectWithTag("Enemy");
         if (enemy!=null)
         {
            return enemy.GetComponent<Enemy_control>();
         }
        else
        {
          return null;
        }
    }

    static Player_Control GetPlayer()
    {
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            return player.GetComponent<Player_Control>();
        }
        else
        {
            return null;
        }
    }

    public void BombGrounded()
    {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Bomb"))
        {
            Bomb_Control bc = go.GetComponent<Bomb_Control>();
            if (bc != null)
            {
                bc.Explode();
                death();
            }
        }
        PlayFinal();
        Enemy_control ec = GetEnemy();
        ec.StopBombing();
        --currentBurt;
        if (currentBurt > 0)
        {
            --currentBurt;
        }

        --lives;
        Player_Control player = GetPlayer();
        if (player != null)
        {
            player.setNumBars(lives);
        }
        GameObject game;
        game = GameObject.FindGameObjectWithTag("Player");
        game.GetComponent<Mouse_script>().enabled = false;
    }
    
    /*public void menos()    para restar puntos
    {
        currentPoints -= bursts[currentBurt - 1].pointsPerBomb;
        actualizar();
    }*/

    public void BombCatched()
    {
        currentPoints += bursts[currentBurt - 1].pointsPerBomb;
        actualizar();
    }

    [HideInInspector]
    public int highscore;

    void actualizar()
    {
        if (points>highscore)
        {
            highscore = points;
        }
    }

    public void adioshigh()
    {
        highscore = 0;
    }


    void Papascha()
    {
        if (lives == 0)
        {

            SceneManager.LoadScene("GameOver");
            SaveScore(highscore);
        }
    }

    public int GetMaxScore()
    {
        return PlayerPrefs.GetInt("PuntBase", highscore);
    }

    public void SaveScore(int puntuacion)
    {
        PlayerPrefs.SetInt("PuntBase", puntuacion);
    }

}
