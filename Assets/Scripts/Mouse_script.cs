﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse_script : MonoBehaviour
{
    [Tooltip("min desplazamiento")]
    public float xMin = -10f;

    [Tooltip("max desplazamiento")]
    public float xMax = 10f;

    public Animator anim;

    private Rigidbody rb;

    private float accMouseMovement = 0.0f;

    //Vector3 lastPos;

    // Start is called before the first frame update
    void Start()
    {
        //lastPos = transform.position;
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            enabled = false;
        }
    }

    void Update()
    {
        accMouseMovement += Input.GetAxisRaw("Mouse X");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       Vector3 currentPos = transform.position;
        currentPos.x += accMouseMovement;
        accMouseMovement = 0.0f;
        /*
        if (currentPos.x==lastPos.x)
        {
            anim.SetFloat("horizontal", 0);
        }
        else
        {
            anim.SetFloat("horizontal", 1);
        }

        lastPos = transform.position;
        */
        if (currentPos.x < xMin)
        {
            currentPos.x = xMin;
        }
        else if (currentPos.x > xMax)
        {
            currentPos.x = xMax;
        }

        //transform.position = currentPos;
        
        rb.MovePosition(currentPos);
        //rb.position = currentPos;
    }

}
