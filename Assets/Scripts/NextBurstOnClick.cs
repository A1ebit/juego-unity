﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextBurstOnClick : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameManager.Instance.NextBurst();

            GameObject game;
            game = GameObject.FindGameObjectWithTag("Player");
            game.GetComponent<Mouse_script>().enabled = true;
        }

    }
}
