﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ojala_Sirva : MonoBehaviour
{
    private static Ojala_Sirva _instance;

    public static Ojala_Sirva Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("New : Error");
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
