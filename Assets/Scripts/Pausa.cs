﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pausa : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public bool isPaused;

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        }
        if (isPaused)
        {
            ActivateMenu();
        }
        else
        {
            DesactivateMenu();
        }
    }
   public void ActivateMenu()
    {
        Cursor.visible = true;

        Time.timeScale = 0;
        pauseMenuUI.SetActive(true);
        GameObject sound;
        sound = GameObject.FindGameObjectWithTag("Sound");
        GameObject game;
        game = GameObject.FindGameObjectWithTag("Player");
        game.GetComponent<NextBurstOnClick>().enabled = false;
        sound.GetComponent<AudioSource>().enabled = false;

    }

   public void DesactivateMenu()
    {
        Cursor.visible = false;

        Time.timeScale = 1;
        pauseMenuUI.SetActive(false);
        isPaused = false;
        GameObject game;
        game = GameObject.FindGameObjectWithTag("Player");
        GameObject sound;
        sound = GameObject.FindGameObjectWithTag("Sound");
        game.GetComponent<NextBurstOnClick>().enabled = true;
        sound.GetComponent<AudioSource>().enabled = true;

    }

}
