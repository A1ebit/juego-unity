﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piso_control : MonoBehaviour
{
    [Tooltip("velocidad de X")]
    public float ScrollX = 0.5f;

    [Tooltip("velocidad de Y")]
    public float ScrollY = 0.5f;

    private AudioSource Audio;

    void Start()
    {
        Audio = GetComponent<AudioSource>();
        if (Audio == null)
        {
            Debug.LogWarning("No tiene Audio");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bomb"))
        {
            /*Bomb_Control bc = other.GetComponent<Bomb_Control>();
            if (bc != null)
            {
                Audio.pitch = Speed2pitcj(bc.speed);
            }
            else
            {
                Audio.pitch = 1.0f;
            }*/
            if (Audio != null)
            {
                Audio.Play();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        float OffsetX = Time.time * ScrollX;
        float OffsetY = Time.time * ScrollY;
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(OffsetX, OffsetY);
    }
}
