﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Control : MonoBehaviour
{
    private AudioSource Audio;
    public GameObject topBar;
    public GameObject midBar;
    public GameObject botBar;
  
    private static Player_Control _instance;
    public static Player_Control Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("New : Error");
            }
            return _instance;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Audio = GetComponent<AudioSource>();
        if (Audio == null)
        {
            Debug.LogWarning("No tiene Audio");
        }

        setNumBars(3);

    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void setNumBars(int num)
    {
        if (num >  3)
        {
            Debug.LogError("No puedes tener mas de 3 vidas");
            return;
        }

        topBar.SetActive(num > 0);
        midBar.SetActive(num > 1);
        botBar.SetActive(num > 2);
        /*
        BoxCollider c = GetComponent<BoxCollider>();
        c.center = new Vector3(0.0f, -0.25f * (num - 1), 0.0f);
        float height;
        if (num!=0)
        {
            height = 0.5f * num - 0.25f;
        }
        else
        {
            height = 0.0f;
        }
        c.size = new Vector3(2.0f, height, 1.0f);*/
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bomb"))
        {
            /*Bomb_Control bc = other.GetComponent<Bomb_Control>();
            if (bc != null)
            {
                Audio.pitch = Speed2pitcj(bc.speed);
            }
            else
            {
                Audio.pitch = 1.0f;
            }*/
            if (Audio != null)
            {
                Audio.Play();
            }
        }
    }
     
    // Update is called once per frame
    void Update()
    {
        
    }

    /*static float Speed2pitcj (float speed)
    {
        float normalised = Math.InverseLerp(2.0f, 8.0f, speed);
        return Mathf.Lerp(0.333f, 1.666f, normalised);
    }*/
}

