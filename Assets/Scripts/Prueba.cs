﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class Prueba : MonoBehaviour
{
    public Transform Puntuaciones;
    public Transform Template;
    string nick1;
    public Text nick;
    public Text puntos;
    public Text name;
    public GameObject BestLuck;
    public GameObject boton;

    string Nombres;
    string Highscores;
    string rankString;

    private void Update()
    {
        nick1 = nick.GetComponent<Text>().text;
        heyadios();
    }

    private void Awake()
    {
        Invoke("reinicio", 45.0f);
    }

    private void ShowScoreBoard()
    {
        boton.SetActive(true);
        Template.gameObject.SetActive(false);


        int primero = PlayerPrefs.GetInt("top1", 10);
        int segundo = PlayerPrefs.GetInt("top2", 8);
        int tercero = PlayerPrefs.GetInt("top3", 6);
        int cuarto = PlayerPrefs.GetInt("top4", 3);
        int quinto = PlayerPrefs.GetInt("top5", 1);
        int ScoreBoard = PlayerPrefs.GetInt("PuntBase");

        string p1 = PlayerPrefs.GetString("rank1", "aaa");
        string p2 = PlayerPrefs.GetString("rank2", "bbb");
        string p3 = PlayerPrefs.GetString("rank3", "ccc");
        string p4 = PlayerPrefs.GetString("rank4", "ddd");
        string p5 = PlayerPrefs.GetString("rank5", "eee");
        string NICK = PlayerPrefs.GetString("rank", nick1);


        if (ScoreBoard > primero)
        {
            quinto = cuarto;
            p5 = p4;
            cuarto = tercero;
            p4 = p3;
            tercero = segundo;
            p3 = p2;
            segundo = primero;
            p2 = p1;
            primero = ScoreBoard;
            p1 = NICK;
            SaveTop1(primero);
            SaveTop2(segundo);
            SaveTop3(tercero);
            SaveTop4(cuarto);
            SaveTop5(quinto);
            SaveRank1(p1);
            SaveRank2(p2);
            SaveRank3(p3);
            SaveRank4(p4);
            SaveRank5(p5);

        }
        else if (ScoreBoard > segundo)

        {
            quinto = cuarto;
            p5 = p4;
            cuarto = tercero;
            p4 = p3;
            tercero = segundo;
            p3 = p2;
            segundo = ScoreBoard;
            p2 = NICK;
            SaveRank2(p2);
            SaveRank3(p3);
            SaveRank4(p4);
            SaveRank5(p5);
            SaveTop2(segundo);
            SaveTop3(tercero);
            SaveTop4(cuarto);
            SaveTop5(quinto);

        }
        else if (ScoreBoard > tercero)

        {
            quinto = cuarto;
            p5 = p4;
            cuarto = tercero;
            p4 = p3;
            tercero = ScoreBoard;
            p3 = NICK;
            SaveTop3(tercero);
            SaveTop4(cuarto);
            SaveTop5(quinto);
            SaveRank3(p3);
            SaveRank4(p4);
            SaveRank5(p5);

        }
        else if (ScoreBoard > cuarto)

        {
            quinto = cuarto;
            p5 = p4;
            cuarto = ScoreBoard;
            p4 = NICK;
            SaveRank4(p4);
            SaveRank5(p5);
            SaveTop4(cuarto);
            SaveTop5(quinto);

        }
        else if (ScoreBoard > quinto)

        {
            quinto = ScoreBoard;
            p5 = NICK;
            SaveTop5(quinto);
            SaveRank5(p5);
        }
        else
        {
            BestLuck.SetActive(true);
            name.text = NICK;
            Highscores = ScoreBoard + "Pts";
            puntos.text = Highscores;
        }

        float templateHeight = 75f;
        for (int i = 0; i < 5; i++)
        {
            Transform entryTransform = Instantiate(Template, Puntuaciones);
            RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
            entryRectTransform.anchoredPosition = new Vector2(-714, -templateHeight * i);
            entryTransform.gameObject.SetActive(true);

            int rank = i + 1;

            switch (rank)
            {
                default:
                    rankString = rank + "TH";
                    Highscores = ScoreBoard + "Pts";
                    Nombres = p1; break;
                case 1:
                    rankString = "1ST";
                    Highscores = primero + "Pts";
                    Nombres = p1; break;

                case 2:
                    rankString = "2ND";
                    Highscores = segundo + "Pts";
                    Nombres = p2; break;

                case 3:
                    rankString = "3RD";
                    Highscores = tercero + "Pts";
                    Nombres = p3; break;

                case 4:
                    rankString = rank + "TH";
                    Highscores = cuarto + "Pts";
                    Nombres = p4; break;

                case 5:
                    rankString = rank + "TH";
                    Highscores = quinto + "Pts";
                    Nombres = p5; break;

            }


            entryTransform.Find("Rank").GetComponent<Text>().text = rankString;
            entryTransform.Find("Score").GetComponent<Text>().text = Highscores;
            entryTransform.Find("Name").GetComponent<Text>().text = Nombres;
        }
    }

    public void ShowScore()
    {
        Invoke("ShowScoreBoard", 0.3f);
    }

    public void reinicio()
    {
        SceneManager.LoadScene("Menu");
    }

    public void heyadios()
    {
        if (Input.GetMouseButtonDown(0))
        {
            BestLuck.SetActive(false);
        }
    }

    public void SaveTop1(int puntuacion)
    {
        PlayerPrefs.SetInt("top1", puntuacion);
    }

    public void SaveTop2(int puntuacion)
    {
        PlayerPrefs.SetInt("top2", puntuacion);
    }

    public void SaveTop3(int puntuacion)
    {
        PlayerPrefs.SetInt("top3", puntuacion);
    }

    public void SaveTop4(int puntuacion)
    {
        PlayerPrefs.SetInt("top4", puntuacion);
    }

    public void SaveTop5(int puntuacion)
    {
        PlayerPrefs.SetInt("top5", puntuacion);
    }

    public void SaveRank1(string nick)
    {
        PlayerPrefs.SetString("rank1", nick);
    }

    public void SaveRank2(string nick)
    {
        PlayerPrefs.SetString("rank2", nick);
    }

    public void SaveRank3(string nick)
    {
        PlayerPrefs.SetString("rank3", nick);
    }

    public void SaveRank4(string nick)
    {
        PlayerPrefs.SetString("rank4", nick);
    }

    public void SaveRank5(string nick)
    {
        PlayerPrefs.SetString("rank5", nick);
    }
}
