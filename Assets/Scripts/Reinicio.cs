﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reinicio : MonoBehaviour
{
    // Start is called before the first frame update
    public void Start()
    {
        Cursor.visible = true;
        GameManager game = GetGame();
        if (game != null)
        {
            game.Restart();
            game.revive();
        }
    }

    static GameManager GetGame()
    {
        GameObject game;
        game = GameObject.FindGameObjectWithTag("GameController");
        if (game != null)
        {
            return game.GetComponent<GameManager>();
        }
        else
        {
            return null;
        }
    }
}
