﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class ScoresTable : MonoBehaviour
{
    public Transform Puntuaciones;
    public Transform Template;
    string nick1;
    public Text nick;
    public Text puntos;
    public Text name;
    public GameObject BestLuck;
    public GameObject boton;

    private void Update()
    {
        nick1 = nick.GetComponent<Text>().text;
        heyadios();
    }

    private void Awake()
    {
        Invoke("reinicio", 45.0f);
    }

    private void ShowScoreBoard()
    {
        boton.SetActive(true);
        Template.gameObject.SetActive(false);

        float templateHeight = 55f;
        for (int i = 0; i < 10; i++)
        {
            Transform entryTransform = Instantiate(Template, Puntuaciones);
            RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
            entryRectTransform.anchoredPosition = new Vector2(-714, -templateHeight * i);
            entryTransform.gameObject.SetActive(true);

            int rank = i + 1;

            int primero = 450;
            int segundo = 426;
            int tercero = 374;
            int cuarto = 365;
            int quinto = 359;
            int sexto = 255;
            int septimo = 142;
            int octavo = 137;
            int noveno = 88;
            int decimo = 50;

            string p1 = "A1E";
            string p2 = "VA1";
            string p3 = "RIC";
            string p4 = "MRL";
            string p5 = "CHR";
            string p6 = "SNA";
            string p7 = "ZAC";
            string p8 = "1EO";
            string p9 = "CE1";
            string p10 = "9SD";

            string Nombres;
            string Highscores;
            string rankString;

            if (GameManager.Instance.highscore > primero)
            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = septimo;
                p8 = p7;
                septimo = sexto;
                p7 = p6;
                sexto = quinto;
                p6 = p5;
                quinto = cuarto;
                p5 = p4;
                cuarto = tercero;
                p4 = p3;
                tercero = segundo;
                p3 = p2;
                segundo = primero;
                p2 = p1;
                primero = GameManager.Instance.highscore;
                p1 = nick1;
            }
            else if (GameManager.Instance.highscore > segundo)

            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = septimo;
                p8 = p7;
                septimo = sexto;
                p7 = p6;
                sexto = quinto;
                p6 = p5;
                quinto = cuarto;
                p5 = p4;
                cuarto = tercero;
                p4 = p3;
                tercero = segundo;
                p3 = p2;
                segundo = GameManager.Instance.highscore;
                p2 = nick1;

            }
            else if (GameManager.Instance.highscore > tercero)

            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = septimo;
                p8 = p7;
                septimo = sexto;
                p7 = p6; ;
                sexto = quinto;
                p6 = p5;
                quinto = cuarto;
                p5 = p4;
                cuarto = tercero;
                p4 = p3;
                tercero = GameManager.Instance.highscore;
                p3 = nick1;

            }
            else if (GameManager.Instance.highscore > cuarto)

            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = septimo;
                p8 = p7;
                septimo = sexto;
                p7 = p6;
                sexto = quinto;
                p6 = p5;
                quinto = cuarto;
                p5 = p4;
                cuarto = GameManager.Instance.highscore;
                p4 = nick1;

            }
            else if (GameManager.Instance.highscore > quinto)

            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = septimo;
                p8 = p7;
                septimo = sexto;
                p7 = p6;
                sexto = quinto;
                p6 = p5;
                quinto = GameManager.Instance.highscore;
                p5 = nick1;

            }
            else if (GameManager.Instance.highscore > sexto)

            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = septimo;
                p8 = p7;
                septimo = sexto;
                p7 = p6;
                sexto = GameManager.Instance.highscore;
                p6 = nick1;

            }
            else if (GameManager.Instance.highscore > septimo)

            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = septimo;
                p8 = p7;
                septimo = GameManager.Instance.highscore;
                p7 = nick1;

            }
            else if (GameManager.Instance.highscore > octavo)

            {
                decimo = noveno;
                p10 = p9;
                noveno = octavo;
                p9 = p8;
                octavo = GameManager.Instance.highscore;
                p8 = nick1;

            }
            else if (GameManager.Instance.highscore > noveno)

            {
                decimo = noveno;
                p10 = p9;
                noveno = GameManager.Instance.highscore;
                p9 = nick1;

            }
            else if (GameManager.Instance.highscore > decimo)
            {
                decimo = GameManager.Instance.highscore;
                p10 = nick1;

            }
            else
            {
                BestLuck.SetActive(true);
                name.text = nick1;
                Highscores = GameManager.Instance.highscore + "Pts";
                puntos.text = Highscores;
            }

            switch (rank)
            {
                default:
                    rankString = rank + "TH";
                    Highscores = GameManager.Instance.highscore + "Pts";
                    Nombres = p1; break;
                case 1:
                    rankString = "1ST";
                    Highscores = primero + "Pts";
                    Nombres = p1; break;

                case 2:
                    rankString = "2ND";
                    Highscores = segundo + "Pts";
                    Nombres = p2; break;

                case 3:
                    rankString = "3RD";
                    Highscores = tercero + "Pts";
                    Nombres = p3; break;

                case 4:
                    rankString = rank + "TH";
                    Highscores = cuarto + "Pts";
                    Nombres = p4; break;

                case 5:
                    rankString = rank + "TH";
                    Highscores = quinto + "Pts";
                    Nombres = p5; break;

                case 6:
                    rankString = rank + "TH";
                    Highscores = sexto + "Pts";
                    Nombres = p6; break;

                case 7:
                    rankString = rank + "TH";
                    Highscores = septimo + "Pts";
                    Nombres = p7; break;

                case 8:
                    rankString = rank + "TH";
                    Highscores = octavo + "Pts";
                    Nombres = p8; break;

                case 9:
                    rankString = rank + "TH";
                    Highscores = noveno + "Pts";
                    Nombres = p9; break;

                case 10:
                    rankString = rank + "TH";
                    Highscores = decimo + "Pts";
                    Nombres = p10; break;

            }


            entryTransform.Find("Rank").GetComponent<Text>().text = rankString;
            entryTransform.Find("Score").GetComponent<Text>().text = Highscores;
            entryTransform.Find("Name").GetComponent<Text>().text = Nombres;
        }
    }

    public void ShowScore()
    {
        Invoke("ShowScoreBoard", 0.3f);
    }

    public void reinicio()
    {
        SceneManager.LoadScene("Menu");
    }

    public void heyadios()
    {
        if (Input.GetMouseButtonDown(0))
        {
            BestLuck.SetActive(false);
        }
    } 
}
