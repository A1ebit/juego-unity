﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unable_things : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject game;
        game = GameObject.FindGameObjectWithTag("Player");
        game.GetComponent<NextBurstOnClick>().enabled = false;
        game.GetComponent<Mouse_script>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
